/*
    SPDX-FileCopyrightText: 2022 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: MIT
*/

use serde::Serialize;
use std::env;
use std::fs::File;
use std::io::Write;
use tinytemplate::TinyTemplate;

use kqt6::project;

struct ProjectData {
    name: String,
    missing_test_platforms: Vec<String>,
}

#[derive(Serialize)]
struct Context {
    projects: Vec<String>,
}

fn main() {
    let mut projects = load_project_data();

    projects.retain(|project| !project.missing_test_platforms.is_empty());

    let template_file: String = env::var("KQT6_TESTS_TEMPLATE").unwrap();

    let template: String = std::fs::read_to_string(template_file).unwrap();

    let project_display_strings = projects
        .iter()
        .map(|project| {
            format!(
                "{}: {}",
                project.name,
                project.missing_test_platforms.join(" ")
            )
        })
        .collect();

    let mut tt = TinyTemplate::new();
    tt.add_template("the_template", &template).unwrap();

    let context = Context {
        projects: project_display_strings,
    };

    let rendered = tt.render("the_template", &context);

    let ouput_file: String = env::var("KQT6_TESTS_OUTPUT").unwrap();

    let mut output_file = File::create(ouput_file).unwrap();

    output_file.write_all(rendered.unwrap().as_bytes()).unwrap();
}

fn load_project_data() -> Vec<ProjectData> {
    let mut result = vec![];

    let projects = project::get_projects(&env::var("KQT6_PROJECTS_FILE").unwrap());

    for project in projects {
        let kdeci = project.kde_ci();
        let gitlabci = project.gitlab_ci();

        if kdeci.is_none() || gitlabci.is_none() {
            continue;
        }

        let required_tests_platforms = match kdeci.unwrap().options {
            None => vec![],
            Some(options) => options.require_passing_tests_on.unwrap_or(vec![]),
        };

        let ci_platforms = ci_platforms(&gitlabci.unwrap().include.unwrap_or_default());

        let missing_platforms = missing_platforms(&ci_platforms, &required_tests_platforms);

        result.push(ProjectData {
            name: project.base_name(),
            missing_test_platforms: missing_platforms,
        });
    }

    return result;
}

fn ci_platforms(includes: &Vec<String>) -> Vec<String> {
    let mut result = vec![];

    for line in includes {
        if line.ends_with("linux.yml") {
            result.push("Linux/Qt5".to_string());
        } else if line.ends_with("linux-qt6.yml") {
            result.push("Linux/Qt6".to_string());
        } else if line.ends_with("freebsd.yml") {
            result.push("FreeBSD/Qt5".to_string());
        } else if line.ends_with("freebsd-qt6.yml") {
            result.push("FreeBSD/Qt6".to_string());
        } else if line.ends_with("windows.yml") {
            result.push("Windows/Qt5".to_string());
        } else if line.ends_with("windows-qt6.yml") {
            result.push("Windows/Qt6".to_string());
        }
        // no Android since we don't run tests there'
    }

    return result;
}

fn missing_platforms(ci_platforms: &Vec<String>, tests_platforms: &Vec<String>) -> Vec<String> {
    let mut result = vec![];

    if tests_platforms.contains(&"@all".to_string()) {
        return result;
    }

    for ci_platform in ci_platforms {
        let found = tests_platforms.iter().find(|test_platform| {
            if test_platform == &ci_platform {
                return true;
            }

            if ci_platform.starts_with(&test_platform.to_string()) {
                return true;
            }

            return false;
        });

        if found.is_none() {
            result.push(ci_platform.to_string());
        }
    }

    return result;
}
