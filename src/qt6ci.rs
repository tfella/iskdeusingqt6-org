/*
    SPDX-FileCopyrightText: 2022 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: MIT
*/

use kqt6::project;
use kqt6::util;
use serde::Serialize;
use std::env;
use std::fs::File;
use std::io::Write;
use tinytemplate::TinyTemplate;

#[derive(Serialize)]
struct Context {
    release_service_projects: Vec<String>,
    extragear_projects: Vec<String>,
    plasma_projects: Vec<String>,
    other_projects: Vec<String>,
    num_building: usize,
    num_total: usize,
}

fn list_projects(file: &str) -> Vec<String> {
    let mut result: Vec<String> = vec![];

    let base = std::env::var("KQT6_SRC_DIR").unwrap();

    if let Ok(lines) = util::read_lines(base + "/" + file) {
        for line in lines {
            if let Ok(project_name) = line {
                result.push(project_name);
            }
        }
    }
    return result;
}

fn main() {
    let all_projects = project::get_projects(&env::var("KQT6_PROJECTS_FILE").unwrap());

    let num_total = all_projects.len();

    let mut missing_names: Vec<String> = all_projects
        .iter()
        .filter(|project| !project.has_qt6())
        .map(|project| project.base_name())
        .collect();

    missing_names.sort();

    let release_service_projects = list_projects("release-service.txt");
    let plasma_projects = list_projects("plasma.txt");
    let extragear_projects = list_projects("extragear.txt");

    let mut missing_release_service_projects = vec![];
    let mut missing_plasma_projects = vec![];
    let mut missing_extragear_projects = vec![];
    let mut missing_other_projects = vec![];

    for project in &missing_names {
        if release_service_projects.contains(&project) {
            missing_release_service_projects.push(project.to_string());
        } else if plasma_projects.contains(&project) {
            missing_plasma_projects.push(project.to_string());
        } else if extragear_projects.contains(&project) {
            missing_extragear_projects.push(project.to_string());
        } else {
            missing_other_projects.push(project.to_string());
        }
    }

    let num_building = num_total - missing_names.len();

    let template_file: String = env::var("KQT6_INDEX_TEMPLATE").unwrap();

    let template: String = std::fs::read_to_string(template_file).unwrap();

    let mut tt = TinyTemplate::new();
    tt.add_template("the_template", &template).unwrap();

    let context = Context {
        release_service_projects: missing_release_service_projects,
        extragear_projects: missing_extragear_projects,
        plasma_projects: missing_plasma_projects,
        other_projects: missing_other_projects,
        num_building,
        num_total,
    };

    let rendered = tt.render("the_template", &context);

    let ouput_file: String = env::var("KQT6_INDEX_OUTPUT").unwrap();

    let mut output_file = File::create(ouput_file).unwrap();

    output_file.write_all(rendered.unwrap().as_bytes()).unwrap();
}
