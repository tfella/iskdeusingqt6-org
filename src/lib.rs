/*
    SPDX-FileCopyrightText: 2022 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: MIT
*/

pub mod ci {

    pub mod kde {

        use serde::Deserialize;
        use serde_yaml::Value;
        use std::collections::BTreeMap as Map;

        #[derive(Deserialize, Default)]
        pub struct KdeCi {
            #[serde(rename = "Dependencies")]
            pub dependencies: Option<Vec<Dependencies>>,
            #[serde(rename = "Options")]
            pub options: Option<Options>,
        }

        #[derive(Deserialize)]
        pub struct Options {
            #[serde(rename = "require-passing-tests-on")]
            pub require_passing_tests_on: Option<Vec<String>>,
        }

        #[derive(Deserialize)]
        pub struct Dependencies {
            pub require: Require,
        }

        #[derive(Deserialize)]
        pub struct Require {
            #[serde(flatten)]
            pub other: Map<String, Value>,
        }
    }

    pub mod gitlab {
        use serde::Deserialize;

        #[derive(Deserialize, Default)]
        pub struct GitlabCi {
            pub include: Option<Vec<String>>,
        }
    }
}

pub mod project {

    use crate::ci::gitlab::GitlabCi;
    use crate::ci::kde::KdeCi;
    use crate::util;
    use std::env;

    #[derive(PartialEq, Eq)]
    pub struct Project {
        base_name: String,
    }

    impl Project {
        pub fn from_full_name(full_name: String) -> Self {
            Project {
                base_name: full_name.split("/").into_iter().nth(1).unwrap().to_string(),
            }
        }

        pub fn from_base_name(base_name: String) -> Self {
            Project { base_name }
        }

        pub fn base_name(&self) -> String {
            self.base_name.to_string()
        }

        pub fn printable_name(&self) -> String {
            self.base_name().replace("-", "_")
        }

        pub fn display_name(&self) -> String {
            self.base_name.to_string()
        }

        pub fn kde_ci(&self) -> Option<KdeCi> {
            let projects_home = env::var("KQT6_PROJECT_HOME").unwrap();

            let maybe_kdeci_file = std::fs::File::open(format!(
                "{}/{}/.kde-ci.yml",
                projects_home,
                self.base_name()
            ));

            if maybe_kdeci_file.is_err() {
                return None;
            }

            return serde_yaml::from_reader(maybe_kdeci_file.unwrap()).unwrap();
        }

        pub fn gitlab_ci(&self) -> Option<GitlabCi> {
            let projects_home = env::var("KQT6_PROJECT_HOME").unwrap();

            let maybe_gitlabci_file = std::fs::File::open(format!(
                "{}/{}/.gitlab-ci.yml",
                projects_home,
                self.base_name()
            ));

            if maybe_gitlabci_file.is_err() {
                return None;
            }

            return serde_yaml::from_reader(maybe_gitlabci_file.unwrap()).unwrap();
        }

        pub fn has_qt6(&self) -> bool {
            let ci_jobs = self
                .gitlab_ci()
                .unwrap_or_default()
                .include
                .unwrap_or_default();

            let has_qt6 = ci_jobs.into_iter().any(|job| job.contains("linux-qt6"));

            return has_qt6;
        }
    }

    pub fn get_projects(file: &str) -> Vec<Project> {
        let mut result = vec![];

        if let Ok(lines) = util::read_lines(file) {
            for line in lines {
                if let Ok(project_name) = line {
                    result.push(Project::from_full_name(project_name));
                }
            }
        }

        return result;
    }
}

pub mod util {

    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    // The output is wrapped in a Result to allow matching on errors
    // Returns an Iterator to the Reader of the lines of the file.
    pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}
