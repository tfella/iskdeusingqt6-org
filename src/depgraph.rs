/*
    SPDX-FileCopyrightText: 2022 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: MIT
*/

use std::env;
use std::fs::File;
use std::io::Write;

use kqt6::project;
use kqt6::project::Project;

fn dependencies(project: &Project) -> Vec<Project> {
    let mut result = vec![];

    for dep in project
        .kde_ci()
        .unwrap_or_default()
        .dependencies
        .unwrap_or_default()
    {
        for (key, _) in dep.require.other {
            result.push(Project::from_full_name(key));
        }
    }

    return result;
}

fn main() {
    let all_projects = project::get_projects(&env::var("KQT6_PROJECTS_FILE").unwrap());

    let missing_projects: Vec<Project> = all_projects
        .into_iter()
        .filter(|project| !project.has_qt6())
        .collect();

    let ouput_file: String = env::var("KQT6_DEPS_OUTPUT").unwrap();

    let maybe_graph_file = File::create(ouput_file);

    if maybe_graph_file.is_err() {
        panic!("Couldn't create graph file");
    }

    let mut graph_file = maybe_graph_file.unwrap();

    graph_file.write_all(b"digraph deps {\n").unwrap();

    for project in &missing_projects {
        graph_file
            .write_all(
                format!(
                    "    {} [label=\"{}\"]\n",
                    project.printable_name(),
                    project.display_name()
                )
                .as_bytes(),
            )
            .unwrap();
    }

    for project in &missing_projects {
        let mut result = dependencies(project);

        result.retain(|it| missing_projects.contains(it));

        for dep in result {
            graph_file
                .write_all(
                    format!(
                        "    {} -> {};\n",
                        project.printable_name(),
                        dep.printable_name()
                    )
                    .as_bytes(),
                )
                .unwrap();
        }
    }

    graph_file.write_all(b"}\n").unwrap();
}
